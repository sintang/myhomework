package com.lagou.dao;

import com.lagou.pojo.User;

import java.util.List;

/**
 * @author sintang
 * @date 2020-09-05 16
 */
public interface IUserDao {

    List<User> findAll();
    User findOne(User user);
    User findByCondition(User user);

    /**
     * 增修删方法
     * @param user
     */
    void insertUser(User user);
    void updateUser(User user);
    void deleteUser(User user);
}
