package com.lagou.test;

import com.lagou.dao.IUserDao;
import com.lagou.io.Resources;
import com.lagou.pojo.User;
import com.lagou.sqlSession.SqlSession;
import com.lagou.sqlSession.SqlSessionFactory;
import com.lagou.sqlSession.SqlSessionFactoryBuilder;
import org.dom4j.DocumentException;
import org.junit.Before;
import org.junit.Test;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;

/**
 * @author sintang
 * @date 2020-09-05 00
 */
public class IPersistenceTest {
    private  SqlSession sqlSession;

    @Before
    public void init() throws PropertyVetoException, DocumentException {
        InputStream inputStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = sqlSessionFactory.openSession();
    }

    @Test
    public void findOne() throws Exception {

        // 查询列表
        User user = new User();
        user.setId(1);
        user = (User)sqlSession.selectOne("com.lagou.dao.IUserDao.findOne", user);
        System.out.println(user);
    }

    @Test
    public void findAll() throws Exception {

        // 查询列表
        User user = new User();
        List<Object> objects = sqlSession.selectList("com.lagou.dao.IUserDao.findAll", user);
        System.out.println(objects);
    }


    @Test
    public void getMapperTest1(){
        User user = new User();
        IUserDao userDao =  sqlSession.getMapper(IUserDao.class);
        List<User> userList = userDao.findAll();
        System.out.println(userList);
    }

    @Test
    public void insert(){
        User user = new User();
        user.setId(4);
        user.setUsername("hello world");
        IUserDao userDao =  sqlSession.getMapper(IUserDao.class);
        userDao.insertUser(user);
    }

    @Test
    public void update(){
        User user = new User();
        user.setId(4);
        user.setUsername("hello java");
        IUserDao userDao =  sqlSession.getMapper(IUserDao.class);
        userDao.updateUser(user);
    }



    @Test
    public void delete(){
        User user = new User();
        user.setId(4);
        IUserDao userDao =  sqlSession.getMapper(IUserDao.class);
        userDao.deleteUser(user);
    }

}
