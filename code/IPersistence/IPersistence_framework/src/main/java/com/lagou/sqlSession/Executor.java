package com.lagou.sqlSession;

import com.lagou.pojo.Configuration;
import com.lagou.pojo.MappedStatement;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author sintang
 * @date 2020-09-03 22
 */
public interface Executor {

    /**
     * 查询方法
     * @param configuration
     * @param mappedStatement
     * @param params
     * @param <E>
     * @return
     */
    <E> List<E> query(Configuration configuration, MappedStatement mappedStatement,Object... params) throws Exception;

    /**
     * 增删改对应的方法
     * @param configuration
     * @param mappedStatement
     * @param params
     * @throws Exception
     */
    void insert(Configuration configuration, MappedStatement mappedStatement,Object... params) throws Exception;
    void update(Configuration configuration, MappedStatement mappedStatement,Object... params) throws Exception;
    void delete(Configuration configuration, MappedStatement mappedStatement,Object... params) throws Exception;
}
