package com.lagou.sqlSession;

import com.lagou.pojo.Configuration;

/**
 * @author sintang
 * @date 2020-09-02 22
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory{

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration);
    }
}
