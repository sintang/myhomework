package com.lagou.pojo;

import com.lagou.constants.StatementIdType;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 配置信息
 * @author sintang
 * @date 2020-09-02 22
 */
public class Configuration {
    /**
     * 数据源
     */
    private DataSource dataSource;

    /**
     * xml映射
     */
    private Map<String,MappedStatement> mappedStatementMap = new HashMap<>();

    /**
     * xml statementid映射，保存statementid对应的操作类型
     * value 值为insert、update、delete 对应枚举值 StatementIdType
     */
    private Map<String, StatementIdType> mappedStatementTypeMap = new HashMap<>();

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MappedStatement> getMappedStatementMap() {
        return mappedStatementMap;
    }

    public void setMappedStatementMap(Map<String, MappedStatement> mappedStatementMap) {
        this.mappedStatementMap = mappedStatementMap;
    }

    public Map<String, StatementIdType> getMappedStatementTypeMap() {
        return mappedStatementTypeMap;
    }

    public void setMappedStatementTypeMap(Map<String, StatementIdType> mappedStatementTypeMap) {
        this.mappedStatementTypeMap = mappedStatementTypeMap;
    }
}
