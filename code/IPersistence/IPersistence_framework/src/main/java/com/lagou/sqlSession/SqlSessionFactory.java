package com.lagou.sqlSession;

/**
 * @author sintang
 * @date 2020-09-02 22
 */
public interface SqlSessionFactory {
    SqlSession openSession();
}
