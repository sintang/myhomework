package com.lagou.sqlSession;

import java.util.List;

/**
 * @author sintang
 * @date 2020-09-02 22
 */
public interface SqlSession {
    /**
     * 查询列表
     * @param statementId
     * @param params
     * @param <E>
     * @return
     */
    <E> List<E> selectList(String statementId,Object... params) throws Exception;

    /**
     * 查询单一对象
     * @param statementId
     * @param params
     * @param <E>
     * @return
     */
    <E> E selectOne(String statementId,Object... params) throws Exception;


    void insert(String statementId,Object... params) throws Exception;
    void update(String statementId,Object... params) throws Exception;
    void delete(String statementId,Object... params) throws Exception;


    /**
     * 为dao接口生成代理实现类
     * @param mapperClass
     * @param <T>
     * @return
     */
    <T> T getMapper(Class<?> mapperClass);
}
