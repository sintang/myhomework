package com.lagou.constants;

/**
 * @author sintang
 * @date 2020-09-05 18
 */
public enum StatementIdType {
    /**
     * xml 操作类型
     */
    select,insert,update,delete
}
