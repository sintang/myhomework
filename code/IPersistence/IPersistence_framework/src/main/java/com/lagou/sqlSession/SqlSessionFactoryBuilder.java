package com.lagou.sqlSession;

import com.lagou.config.XmlConfigerBuilder;
import com.lagou.pojo.Configuration;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * @author sintang
 * @date 2020-09-02 22
 */
public class SqlSessionFactoryBuilder {


    public SqlSessionFactory build(InputStream in) throws PropertyVetoException, DocumentException {
        // 解析sqlMapConfig.xml、Mapper.xml到Config对象中
        XmlConfigerBuilder xmlConfigerBuilder = new XmlConfigerBuilder();
        Configuration configuration = xmlConfigerBuilder.parseConfig(in);

        // 创建sqlSessionFactory对象：工厂类：生产sqlSession:会话对象
        DefaultSqlSessionFactory defaultSqlSessionFactory = new DefaultSqlSessionFactory(configuration);
        return defaultSqlSessionFactory;
    }
}
